--
-- PostgreSQL database dump
--

-- Dumped from database version 10.2
-- Dumped by pg_dump version 10.2

-- Started on 2018-05-24 14:35:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 218 (class 1255 OID 16520)
-- Name: bi_quizinfo_function(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION bi_quizinfo_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 select QUIZINFO_SEQ.nextval into NEW.QUIZNAME from dual;
 RETURN NEW;
 END;
$$;


--
-- TOC entry 214 (class 1255 OID 16521)
-- Name: bi_quizques_function(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION bi_quizques_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
   select QUIZQUES_SEQ.nextval into NEW.QID from dual;
 RETURN NEW;
 END;
$$;


SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16522)
-- Name: etudiant; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE etudiant (
    id_user integer NOT NULL,
    niveau integer
);


--
-- TOC entry 206 (class 1259 OID 16577)
-- Name: evaluation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE evaluation (
    id_evaluation integer NOT NULL,
    id_texte integer,
    note integer,
    id_enfant integer
);


--
-- TOC entry 207 (class 1259 OID 16580)
-- Name: evaluation_id_evaluation_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE evaluation_id_evaluation_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 207
-- Name: evaluation_id_evaluation_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE evaluation_id_evaluation_seq OWNED BY evaluation.id_evaluation;


--
-- TOC entry 197 (class 1259 OID 16530)
-- Name: question; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE question (
    question_id integer NOT NULL,
    text_id integer,
    enonce text NOT NULL,
    reponse text NOT NULL,
    choix1 text NOT NULL,
    choix2 text NOT NULL,
    choix3 text NOT NULL
);


--
-- TOC entry 198 (class 1259 OID 16536)
-- Name: question_sequance; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE question_sequance
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 199 (class 1259 OID 16538)
-- Name: quizcontact; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE quizcontact (
    name character varying(100),
    email character varying(100),
    phone character varying(100),
    message character varying(100)
);


--
-- TOC entry 200 (class 1259 OID 16541)
-- Name: quizregister; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE quizregister (
    username character varying(100),
    userpass character varying(100),
    category character varying(100),
    email character varying(100),
    id_user integer NOT NULL
);


--
-- TOC entry 201 (class 1259 OID 16544)
-- Name: quizregister_id_user_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE quizregister_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 201
-- Name: quizregister_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE quizregister_id_user_seq OWNED BY quizregister.id_user;


--
-- TOC entry 202 (class 1259 OID 16546)
-- Name: son_of; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE son_of (
    id_enfant integer,
    id_parent integer
);


--
-- TOC entry 203 (class 1259 OID 16549)
-- Name: texte; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE texte (
    domaine character varying(25),
    contenu text,
    id_text integer NOT NULL,
    id_expert integer,
    titre text,
    niveau integer
);


--
-- TOC entry 204 (class 1259 OID 16555)
-- Name: texte_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE texte_sequence
    START WITH 29
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 205 (class 1259 OID 16557)
-- Name: user_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_sequence
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2707 (class 2604 OID 16582)
-- Name: evaluation id_evaluation; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY evaluation ALTER COLUMN id_evaluation SET DEFAULT nextval('evaluation_id_evaluation_seq'::regclass);


--
-- TOC entry 2706 (class 2604 OID 16560)
-- Name: quizregister id_user; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY quizregister ALTER COLUMN id_user SET DEFAULT nextval('quizregister_id_user_seq'::regclass);


--
-- TOC entry 2837 (class 0 OID 16522)
-- Dependencies: 196
-- Data for Name: etudiant; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO etudiant (id_user, niveau) VALUES (6, 1);
INSERT INTO etudiant (id_user, niveau) VALUES (4, 3);
INSERT INTO etudiant (id_user, niveau) VALUES (5, 2);
INSERT INTO etudiant (id_user, niveau) VALUES (8, 1);
INSERT INTO etudiant (id_user, niveau) VALUES (11, 1);
INSERT INTO etudiant (id_user, niveau) VALUES (9, 4);


--
-- TOC entry 2847 (class 0 OID 16577)
-- Dependencies: 206
-- Data for Name: evaluation; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (3, 2, 1, 1);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (7, 4, 1, 1);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (8, 6, 1, 1);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (9, 1, 2, 1);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (11, 2, 0, 10);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (12, 1, 2, 10);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (15, 4, 2, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (16, 6, 1, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (17, 5, 1, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (19, 2, 1, 5);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (21, 3, 1, 5);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (22, 4, 1, 5);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (23, 1, 2, 6);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (24, 32, 1, 3);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (25, 3, 0, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (26, 1, 2, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (1, 1, 3, 9);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (2, 2, 2, 9);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (4, 2, 2, 4);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (5, 1, 4, 5);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (6, 4, 3, 9);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (7, 1, 1, 11);
INSERT INTO evaluation (id_evaluation, id_texte, note, id_enfant) VALUES (8, 5, 4, 9);


--
-- TOC entry 2838 (class 0 OID 16530)
-- Dependencies: 197
-- Data for Name: question; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (1, 1, 'La dactylographie est l''aptitude a', 'saisir un texte sur un clavier d''ordinateur', 'courir trés vite', 'ecrire au stylo', 'dessiner sur un ordinateur');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (2, 1, 'Une bonne dextérité permet d''éviter', 'certains troubles musculaires', 'la gripe', 'la tendinite', 'la migraine');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (3, 1, 'La dactylographie répartie la charge de chaque doigt sur', 'l''ensemble des touches du clavier', 'L''écran', 'L''écran tactile ', 'L''ensemble des touches de la souris ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (4, 1, 'Une bonne dextérité n''épargne pas :', 'les pieds', 'la fatigue ', 'les yeux ', 'les cervicales');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (5, 2, 'La téléphonie mobile c''est ', 'une infrastructure de télécommunication', 'Les smartphone', 'le wifi', 'des applications');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (6, 2, 'Les premiers systémes mobiles fonctionnaient en ', 'mode analogique', 'mode numerique', 'mode triphasé', 'mode imbriqué ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (7, 3, 'de quoi les plantes n''ont pas besoin', 'du sel', 'des sels mineraux ', 'de l''eau ', 'du dioxyde de carbonne');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (8, 3, 'La photosynthése est le processus  qui permet', 'de synthétiser de la matière organique', 'de synthétiser de la matière minérale', 'de synthétiser du bois', 'de synthétiser des légumes ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (9, 4, 'La nasa est l''agence gouvernementale responsable du programmespatial ', 'des Etats-Unis d''Amérique', 'de l''Algérie', 'de l''Autriche', 'de l''Allemagne ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (10, 4, ' Pour féter ses cinquante ans, la NASA a diffusé le 4 février 2008 dans le cosmos la chanson des Beatles ', 'Across the universe', 'Michelle', 'Yesterday ', 'Jude');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (11, 4, 'La NASA comprend', '18 centres de recherches', '17 centres de recherches', '12 centres de recherches', '28 centres de recherches');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (12, 4, 'La nasa a fété ces cinquante ans le ', '4 février 2008', '4 février 1998', '24 février 2010 ', '4 mars 2008');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (13, 5, 'Le parc du Grand Canyon du Colorado est situé', 'sud-ouest des Etats-Unis', 'sud-est des Etats-Unis', 'nord-ouest des Etats-Unis', 'sud-est des Etats-Unis');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (14, 5, 'Dans quel états le Grand Canyon se situe', 'Arizona', 'Colorado', 'New York ', 'Alaska');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (15, 5, 'l''endroit est le résultat spectaculaire du travail', 'de l''érosion', 'de la pluie ', 'des scéismes', 'de la chaleur');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (16, 5, 'Quel fleuve coule en contre-bas', 'le Colorado', 'le mississipi ', 'le gange', 'l''amazone');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (17, 6, 'La marée est causé par', 'la forces de gravitation de la lune et du solei ', 'les courants marins', 'le vent', 'les séismes');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (18, 6, 'Les marées les plus faibles de l''année se produisent', 'aux solstices ', 'aux équinoxes', 'le 1er janvier', 'le 31 décembre');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (19, 7, 'Le prix Nobel est', 'une récompense internationale', 'une course de voiture ', 'un tournois de tennis', 'la coupe du monde de curling');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (20, 7, 'La quel des catégories suivantes ne fais pas partie de celles du prix Nobel', 'Mathématique', 'Physique ', 'Paix', 'La littérature ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (21, 7, 'Le prix Nobel est décernée chaque', 'année', '6 mois ', '4 ans', '3 ans');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (22, 7, 'Ce prix porte le nom d''un chimiste', 'Suédois', 'Francais', 'Allemang', 'Polonais');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (23, 8, 'Le Livre Guinness est ', 'un livre de record ', 'un livre de cuisine', 'un livre de paysages', 'un livre de contes');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (24, 8, 'Le Livre Guinness est paru la première fois en', '1955', '1855', '2005', '1999');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (25, 8, 'Le livre Guinness publié une fois par', 'ans', '6 mois', '3 ans ', '5 ans ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (26, 8, 'Durant une partie de chasse, Sir Hugh Beaver vit passer ', 'Un oiseau', 'Un chien', 'Un chat', 'Un lion');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (27, 9, 'Le sudoku est', 'un jeux en forme de grille ', 'un sport qui se joue avec un balon', 'une technique de concentration', ' un pays');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (28, 9, 'Les symboles dans une grille de Sodoku sont', 'des chiffres allant de 1 a 9', 'des lettres de A a D', 'des chiffres allant de 0 a 9', 'des symboles chinois ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (29, 10, 'Quel épreuve ne fait pas partie de l''Ironman ', 'Le tir a l''arc', '3.8 km de natation ', '180 km de cyclisme', 'un marathon ');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (30, 10, 'Le championnat du monde Ironman WTC a lieu  ', 'a Hawaia Hawai', 'en Algerie', 'au Japon', 'au Botswana');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (31, 11, 'Les agriculteurs qui pratiquent ce type d''agriculture ne misent pas sur ', 'les engrais chimiques ', 'le compostage', 'l''engrais vert', 'la rotation des cultures');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (32, 11, ' L''agriculture biologique est basée sur ', 'l''interdiction de la chimie de synthése', 'l''interdiction des insectes ', 'l''interdiction des animaux', 'l''interdiction des tracteurs');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (37, 39, 'Quest N1 ', 'rep', 'choi', 'chi2', 'chi3');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (38, 40, 'Quest N1 ', 'rep 1', 'chio 3', 'chourba', 'frite');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (39, 40, 'question N2', 'rep N2', 'chiox', 'hafidi', 'sdfjqk');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (40, 41, 'quest nn1', 'rep 01', 'chois 1', 'choi 2 ', 'chois 3');
INSERT INTO question (question_id, text_id, enonce, reponse, choix1, choix2, choix3) VALUES (41, 41, 'quest nn 2', 'rep 01 &#224;&#224;', 'chois 001', 'choi 200 ', 'chois00 3');


--
-- TOC entry 2840 (class 0 OID 16538)
-- Dependencies: 199
-- Data for Name: quizcontact; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2841 (class 0 OID 16541)
-- Dependencies: 200
-- Data for Name: quizregister; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('aa', 'aa', 'Student', 'aa@aa.de', 2);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('ff', 'ff', 'Teacher', 'em', 3);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('enfant1', '1234', 'Student', 'aa@aa.de', 4);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('enfant2', '1234', 'Student', 'aa@aa.de', 5);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('enfant3', '1234', 'Student', 'aa@aa.de', 6);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('parent1', '1234', 'Parent', 'aa@aa.de', 7);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('enfant4', '1234', 'Apprenant', '@@hhghgc', 8);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('amine', '1234', 'Student', 'amine95@gmail.com', 9);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('Yacine', '1234', 'Parent', 'djema@esi.dz', 10);
INSERT INTO quizregister (username, userpass, category, email, id_user) VALUES ('walid', '1234', 'Student', 'ewalid@esi.dz', 11);


--
-- TOC entry 2843 (class 0 OID 16546)
-- Dependencies: 202
-- Data for Name: son_of; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO son_of (id_enfant, id_parent) VALUES (4, 7);
INSERT INTO son_of (id_enfant, id_parent) VALUES (5, 7);
INSERT INTO son_of (id_enfant, id_parent) VALUES (6, 7);


--
-- TOC entry 2844 (class 0 OID 16549)
-- Dependencies: 203
-- Data for Name: texte; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Science', 'La dactylographie est le métier, ou plus simplement l''aptitude à saisir un
texte sur un clavier d''ordinateur (ou anciennement de machine à écrire)
sans le regarder en utilisant ses dix doigts avec rapidité, fluidité et
précision. Une bonne dextérité sur un poste de travail ergonomique
amoindrit la fatigue et l''usure : elle épargne les yeux et les cervicales
si on regarde constamment ce qui se passe à l''écran ou sur la feuille. Elle
permet d''éviter certains troubles musculaires au niveau des mains, que de
mauvaises habitudes de frappe pourraient engendrer, en répartissant par
exemple équitablement la charge de chaque doigt sur l''ensemble des touches
du clavier.
', 1, 1, 'La dactylographie', 1);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Science', 'La téléphonie mobile est une infrastructure de télécommunication qui permet
de communiquer par téléphone sans être relié par câble à un central. Les
premiers systèmes mobiles fonctionnaient en mode analogique. Les terminaux
étaient de taille importante, seulement utilisable dans les automobiles où
ils occupaient une partie du coffre et profitaient de l''alimentation
électrique du véhicule. Les systèmes mobiles actuels fonctionnent en mode
numérique : la voix est échantillonnée, numérisée et transmise sous forme
de bits, puis resynthétisée au niveau de la réception. Les progrès de la
microélectronique ont permis de réduire la taille des téléphones mobiles à
un format de poche.', 2, 2, 'La téléphonie mobile', 1);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Nature', 'La photosynthèse est le processus bioénergétique qui permet aux plantes et
à certaines bactéries de synthétiser de la matière organique en exploitant
la lumière du soleil. Les besoins nutritifs de ces organismes sont du
dioxyde de carbone, de l''eau et des sels minéraux. La photosynthèse est à
la base de l''autotrophie de ces organismes.
La photosynthèse est la principale voie de transformation du carbone
minéral en carbone organique. En tout, les organismes photosynthétiques
assimilent environ 100 milliards de tonnes de carbone en biomasse, chaque
année. Une conséquence importante est la libération de molécules de
dioxygène.', 3, 1, 'La photosynthèse', 1);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Espace', 'La "National Aeronautics and Space Administration" (Administration
nationale de l''aéronautique et de l''espace) plus connue sous son
abréviation NASA, est l''agence gouvernementale responsable du programme
spatial des Etats-Unis d''Amérique. La NASA comprend 18 centres de
recherches sur le sol américain dont le fameux Ames Research Center (centre
de recherches) de Mountain View ou encore le Jet Propulsion Laboratory de
Pasadena (laboratoire de propulsion). Pour fêter ses cinquante ans, la NASA
a diffusé le 4 février 2008 dans le cosmos la chanson des Beatles "Across
the universe", une transmission orientée en direction de l''étoile polaire
(Polaris), la plus brillante de la constellation de la Petite Ourse.', 4, 2, 'La "National Aeronautics and Space Administration"', 2);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Géographie
', 'Le parc du Grand Canyon du Colorado est situé au sud-ouest des Etats-Unis,
au nord de l''état de l''Arizona. Le Grand Canyon n''est ni le plus profond,
ni le plus imposant des canyons terrestres : le Barranca del Cobre, situé
au nord du Mexique et le Hells Canyon dans l''Idaho sont des gorges plus
profondes, mais le site est remarquable pour les points de vue offerts aux
visiteurs et aux couches géologiques qui apparaissent sur les falaises,
bien connues des géologues. Les strates racontent l''histoire du continent
nord-américain. D''autre part, l''endroit est le résultat spectaculaire du
travail de l''érosion, notamment celle du fleuve Colorado qui coule en
contre-bas.', 5, 1, 'Grand Canyon', 3);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Nature', 'La marée est le mouvement montant puis descendant des eaux des mers et des
océans causé par l''effet conjugué des forces de gravitation de la Lune et
du Soleil. Lorsque la Terre et ces deux astres sont sensiblement dans le
même axe, c''est-à-dire lors de la pleine lune et de la nouvelle lune,
ceux-ci agissent de concert et les marées sont de plus grande amplitude
(vives eaux) ; au contraire, lors du premier et du dernier quartier,
l''amplitude est plus faible (mortes eaux). Selon l''endroit, le cycle du
flux et du reflux peut avoir lieu une fois ou deux fois par jour. Les
marées les plus faibles de l''année se produisent normalement aux solstices
d''hiver et d''été, les plus fortes aux équinoxes.', 6, 1, 'La marée', 2);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Culture générale
', 'Le prix Nobel est une récompense internationale décernée chaque année à des
personnes ayant apporté un "grand bénéfice à l''humanité". Ce prix porte
le nom du chimiste suédois Alfred Nobel, qui a légué toute sa fortune pour
la création de ce prix. Il existe en réalité plusieurs prix, car cette
récompense est remise dans 5 domaines différents : la physique, la chimie,
la médecine, la littérature et la paix. Les personnes qui reçoivent un prix
Nobel se sont illustrées dans leur discipline grâce à une invention, une
découverte, un travail... Le premier prix Nobel a été remis en 1901. Depuis,
les prix sont décernés tous les ans, au mois d''octobre. La remise des prix
a lieu le jour anniversaire de la mort d''Alfred Nobel, le 10 décembre.', 7, 2, 'Le prix nobel', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Culture générale', 'Le Livre Guinness des records est un livre de référence, publié une fois par
an et recensant une collection de records du monde reconnus au niveau
international, qui sont à la fois des prouesses humaines et naturelles.
Le livre est paru pour la première fois en 1955 à l''initiative de Norris et
Ross McWhirter suite à une idée de Sir Hugh Beaver de la brasserie Guinness.
Durant une partie de chasse, Sir Hugh Beaver vit passer un oiseau et il fut
impressionné par sa vitesse. Le soir dans un pub entre amis, ils débattirent
pendant un moment sur l''oiseau que Sir Hugh Beaver avait aperçu et quel est
l''oiseau le plus rapide au monde. Après le débat, ils décidèrent de
recenser tous les animaux les plus rapides, les plus lourds, etc.', 8, 1, 'Lelivre Guinness', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Culture ', 'Le sudoku est un jeu en forme de grille défini en 1979 et inspiré du carré
latin ainsi que du problème des 36 officiers du mathématicien suisse
Leonhard Euler. Le but du jeu est de remplir cette grille avec une série de
chiffres (ou de lettres ou de symboles) tous différents, qui ne se trouvent
jamais plus d''une fois sur une même ligne, dans une même colonne ou dans
une même sous-grille. La plupart du temps, les symboles sont des chiffres
allant de 1 à 9, les sous-grilles étant alors des carrés de 3x3. Quelques
symboles sont généralement déjà disposés dans la grille, ce qui facilite un
peu la résolution du problème complet. Des grilles sont publiées dans des
journaux, mais peuvent aussi être générées par ordinateur.
', 9, 2, 'Le SUDOKU', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Sport', 'L''Ironman est le plus long format de triathlon. Les compétitions Ironman
sont des courses consistant à enchaîner 3,8 km de natation, 180 km de
cyclisme puis un marathon (42,195 km) en course à pied. Cette ligue privée
organise un circuit de courses sur distances Ironman, et les championnats du
monde de la distance. Le championnat du monde Ironman WTC est un événement
annuel qui a lieu en octobre à Hawaï. Les concurrents de cette compétition se
tenant à Kailua-Kona (Hawaï) doivent parcourir 3,86 kilomètres à la nage
(traversée de la baie de Kailua-Kona) suivis de 180,2 kilomètres à vélo
(aller-retour de Keauhou à Hawi) puis un marathon le long de la côte de Big
Island (de Keauhou vers Keahole Point vers Kailua-Kona).', 10, 1, 'L''Ironman', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Science', 'L''agriculture biologique est une méthode de production agricole basée sur
l''interdiction de la chimie de synthèse, que ce soit pour les traitements
de protection des plantes ou les engrais. Cette méthode affirme mieux
respecter le vivant et les cycles naturels. Elle vise à gérer de façon
globale la production en favorisant l''agrosystème mais aussi la biodiversité,
les activités biologiques des sols et les cycles biologiques. Les
agriculteurs qui pratiquent ce type d''agriculture misent, par exemple, sur la
rotation des cultures, l''engrais vert, le compostage, la lutte biologique,
l''utilisation de produits naturels et le sarclage mécanique pour maintenir la
productivité des sols et le contrôle des maladies et des parasites.
', 11, 2, 'L''agriculture biologique', 3);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('test', 'test', 30, 3, 'test', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('ff', 'test', 31, 3, 'ff', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('test', 'test', 32, 3, 'test', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('test', 'test', 33, 3, 'test', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('test2', 'test2', 34, 3, 'test2', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('azert', 'azert', 35, 3, 'azert', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('azert', 'azert', 36, 3, 'azert', 4);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('sghf', 'fsfdh', 37, 3, 'hjgghdg', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('sghf', 'fsfdh', 38, 3, 'hjgghdg', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Science', 'Les informations collectées doivent &#234;tre validées,
organisées et consignées sur un support adéquat pour en
faciliter par la suite le traitement
&#8226; Les logiciels de veille facilitent l&#8217;organisation des informations
et le stockage des données collectées
&#8226; L''utilisation des logiciels permet de faciliter la collecte
des informations
&#8226; Moteurs de recherche, les outils de surveillance de sites,
les crawlers, les annuaires, les agents intelligents, etc', 39, 3, 'Quiz', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Sci', 'Texte va ici', 40, 3, 'Quiz 15h', 5);
INSERT INTO texte (domaine, contenu, id_text, id_expert, titre, niveau) VALUES ('Science', 'Les informations collectées doivent &#234;tre validées,
organisées et consignées sur un support adéquat pour en
faciliter par la suite le traitement
&#8226; Les logiciels de veille facilitent l&#8217;organisation des informations
et le stockage des données collectées
&#8226; L''utilisation des logiciels permet de faciliter la collecte
des informations
&#8226; Moteurs de recherche, les outils de surveillance de sites,
les crawlers, les annuaires, les agents intelligents, etc', 41, 3, 'Science 15h41', 5);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 207
-- Name: evaluation_id_evaluation_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('evaluation_id_evaluation_seq', 8, true);


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 198
-- Name: question_sequance; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('question_sequance', 41, true);


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 201
-- Name: quizregister_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('quizregister_id_user_seq', 11, true);


--
-- TOC entry 2859 (class 0 OID 0)
-- Dependencies: 204
-- Name: texte_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('texte_sequence', 41, true);


--
-- TOC entry 2860 (class 0 OID 0)
-- Dependencies: 205
-- Name: user_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_sequence', 3, false);


--
-- TOC entry 2709 (class 2606 OID 16562)
-- Name: etudiant etudiant_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY etudiant
    ADD CONSTRAINT etudiant_pkey PRIMARY KEY (id_user);


--
-- TOC entry 2711 (class 2606 OID 16566)
-- Name: question firstkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY question
    ADD CONSTRAINT firstkey PRIMARY KEY (question_id);


--
-- TOC entry 2713 (class 2606 OID 16568)
-- Name: quizregister quizregister_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY quizregister
    ADD CONSTRAINT quizregister_pkey PRIMARY KEY (id_user);


--
-- TOC entry 2715 (class 2606 OID 16570)
-- Name: texte text_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY texte
    ADD CONSTRAINT text_pkey PRIMARY KEY (id_text);


-- Completed on 2018-05-24 14:35:13

--
-- PostgreSQL database dump complete
--

